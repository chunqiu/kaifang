#!/usr/bin/env python
# -*- coding: utf-8 -*-


import tornado.web

from urls import urls
import os

SETTINGS = {
    "template_path": os.path.join(os.path.dirname(__file__), 'templates'),
}

application = tornado.web.Application(urls)


if __name__ == '__main__':
    print application.handlers
