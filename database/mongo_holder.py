#!/usr/bin/env python
# -*- coding: utf-8 -*-


from pymongo import MongoClient
from mongokit import Connection
from settings import config


class MongoSimpleHolder(MongoClient):


    def __init__(self, **kwargs):

        kwargs = kwargs or {
            'host': 'localhost',
            'port': 27017
        }
        kwargs = kwargs.copy()
        for k in kwargs.keys():
            if not k in ('host', 'port'):
                kwargs.pop(k)
        super(MongoSimpleHolder, self).__init__(**kwargs)


class MongoKitHolder(Connection):

    def __init__(self, **kwargs):

        kwargs = kwargs or {
            'host': 'localhost',
            'port': 27017
        }
        kwargs = kwargs.copy()
        for k in kwargs.keys():
            if not k in ('host', 'port'):
                kwargs.pop(k)
        super(MongoKitHolder, self).__init__(**kwargs)



class Mongo(object):


    @property
    def m(cls):
        return MongoSimpleHolder()