#!/usr/bin/env python
# -*- coding: utf-8 -*-


from handlers import *


urls = [
        (r'/?', IndexHandler),
        (r'/code?', CodeHandler),
        ("/(.*)", StaticHandler),

    ]

if __name__ == '__main__':
    print urls
