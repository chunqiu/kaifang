#!/usr/bin/env python
# -*- coding: utf-8 -*-


import tornado.ioloop
import tornado.httpserver
from tornado.options import define, options

from settings import config, env
from app import application


define("env", default=env, help="Run server in develop|test|product mode.")
define("port", default=config.port, help="Run server on a specific port.", type=int)

if __name__ == "__main__":
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    print 'Server is running at http://127.0.0.1:%d in %s mode.' % (options.port, options.env.capitalize())
    print 'Quit the server with CONTROL-C.\n'
    tornado.ioloop.IOLoop.instance().start()
