#!/usr/bin/env python
# -*- coding: utf-8 -*-


__author__ = 'Dylan'


from collections import OrderedDict
import sys
import os
from utils.tools import load_config


PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))

# 用于切换开发模式：调试、测试、生产环境
#ENV='debug' # debug/test/product/default
# 加载配置文件
CONFIG_DIR = os.path.join(PROJECT_PATH, 'config')
__current_config_path = ''
ENVS = ['debug', 'test', 'product', 'default']
envs = OrderedDict()
for env in ENVS:
    __config_path = os.path.join(CONFIG_DIR, '.'.join([env, 'config.toml']))
    if os.path.exists(__config_path):
        envs[env] = __config_path
assert envs.keys()
env = os.environ.get('ENV', '')
if env:
    assert env in envs.keys(), u'ENV must be in %s or available config for %s' % (ENVS, env)
    __current_config_path = envs[env]
else:
    env, __current_config_path = envs.popitem(last=False)

config = load_config(__current_config_path)

#project path
config.project_path = PROJECT_PATH

#依赖库
if config.libs:
    for v in config.libs.values():
        assert v is not None
        path = os.path.join(PROJECT_PATH, v)
        if os.path.exists(path):
            if v not in sys.path:
                sys.path.append(path)
        else:
            print 'lib dependency %s not found!', v

#日志处理
log_path = os.path.join(PROJECT_PATH, 'logs')
for path in (log_path, ):
    if not os.path.exists(path):
        os.makedirs(path)
import logging
format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
log = os.path.join(log_path, '%s.log' % config.name)
if env in ['debug', 'test']:
    level = logging.DEBUG
else:
    level = logging.INFO
#logging.basicConfig(filename=log, format=format, level=level)
handlers = [logging.FileHandler(log),
             logging.StreamHandler()]
logging.basicConfig(handlers=handlers, format=format, level=level)


import time
os.environ["TZ"] = "Asia/Shanghai"
try:
    time.tzset()
except AttributeError:
    pass


if __name__ == '__main__':
    print 'Project path is %s' % PROJECT_PATH
    print 'Project run mode is %s' % env
    print config
