#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.gen
import tornado.template
import os.path

from settings import config


class StaticHandler(tornado.web.RequestHandler):


    def get(self, template_path, **kwargs):
        '''
        渲染页面
        :param template_path:
        :return:
        '''
        static_base = os.path.join(config.project_path, 'statics')
        loader = tornado.template.Loader(static_base)

        html = loader.load(template_path).generate(**kwargs)
        if template_path[:3] == 'js/':
            self.set_header("Content-Type", "application/javascript; charset=UTF-8")
        elif template_path[:4] == 'css/':
            self.set_header("Content-Type", "text/css; charset=UTF-8")
        elif template_path[:7] == 'images/':
            self.set_header("Content-Type", "image/jpg")

        self.write(html)
        self.finish()
