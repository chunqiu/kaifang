#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime

import tornado.web
import tornado.gen
import tornado.template
import os.path

from settings import config


class BaseHandler(tornado.web.RequestHandler):


    def render(self, template_path, **kwargs):
        '''
        渲染页面
        :param template_path:
        :return:
        '''
        template_base = os.path.join(config.project_path, 'templates')
        loader = tornado.template.Loader(template_base)
        html = loader.load(template_path).generate(**kwargs)

        self.set_header("Content-Type", "text/html; charset=UTF-8")
        self.write(html)
        self.finish()
