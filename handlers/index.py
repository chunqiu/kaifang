#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.gen
import tornado.template
from .base import BaseHandler
from models import pm
from utils.tools import Dict2Object
import json
from redis import Redis

r = Redis()

class IndexHandler(BaseHandler):

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        criteria = {}
        result = []
        name = self.get_argument('name', default='')
        idcard = self.get_argument('idcard', default='')
        phone = self.get_argument('phone', default='')
        code = self.get_argument('code', default='')
        if r.get(code) == '1':
            if name:
                criteria['name'] = name
            if idcard:
                try:
                    criteria['ctfid'] = int(idcard)
                except:
                    criteria['ctfid'] = idcard
            if phone:
                try:
                    criteria['mobile'] = phone
                except:
                    pass

            if criteria:
                result = pm.kaifang.user.find(criteria)
        self.render('index.html', result=result, name=name, idcard=idcard, phone=phone, code=code)

class CodeHandler(BaseHandler):

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def get(self):
        self.render('code.html', result='test')

    @tornado.web.asynchronous
    @tornado.gen.coroutine
    def post(self):
        name = self.get_argument('name', default=None)
        idcard = self.get_argument('idcard', default=None)
        phone = self.get_argument('phone', default=None)
        if name == 'descusr' and idcard == 'descusr' and phone == '15914926715':
            from uuid import uuid1
            code = uuid1().hex[:10]
            r.set(code, '1')
            r.expire(code, 3600 * 3)
            self.write(code)
        else:
            self.render('code.html', result='test', name=name, idcard=idcard, phone=phone)